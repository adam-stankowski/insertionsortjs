/**
 * This simple app demonstrates the insertion sort algorithm and uses basic
 * javascript operations. It generates an array of Items and based on them
 * adds Span elements to DOM. Span elements' height corresponds to item's value.
 *  The algorighm then sorts the array and moves the Span elements around to visualize 
 *  how sorted elements are aligned after insertion sort is applied. 
 *  Insertion sort is a quadratic algorithm, so really slow but it shows that
 *  for small portions of data it's ok. Insertion sort can be used to boost performance
 *  of mergesort sorting subarrays of size 7 or less. 
 */

/* Pickup the form and add event listener on submit. This is prefered to 
 *  polluting the html code with javascript like onsubmit="". 
 */
var form = document.querySelector('div.container div.params form');
form.addEventListener('submit', drawItems);

/*
 * Custom "class" to represent sorted items and practice custom object creation
 * in Javascript
 */
function Item(id, value){
	this.id = id;
	this.value = value;
}

/*
 * Adds a function to Node class which removes all child elements from DOM
 */
Node.prototype.removeAll = function(){
	while(this.firstChild)
		this.removeChild(this.firstChild);
}

/*
 * Sets up a new sort and inserts the DOM items it requires to the page
 * This function groups all things that happen before and while sorting.
 */
function drawItems(evt){
	
	var sortedDiv = document.getElementById("sorted");
	var unsortedDiv = document.getElementById('default');
	let maxItems = document.getElementById('numElems').value;
	sortedDiv.removeAll();
	unsortedDiv.removeAll();
	
	let items = generateItems(maxItems,sortedDiv,unsortedDiv);	
	
	insertionSort(items, sortedDiv);
	evt.preventDefault(); // stop propagating the Submit event and reloading the page
}

/*
 * Generates Items to be sorted and adds them to both 
 * sorted and unsorted div in the order which they appear
 */
function generateItems(maxItems,sortedDiv,unsortedDiv){
	
	var items = new Array();
	
	for(let i = 0; i < maxItems; i++){
		let item = new Item(i,Math.floor(Math.random()* 100)+ 1);
		let elem = document.createElement("span");
		elem.className='item';
		elem.id = i;
		elem.style = 'height:'+ item.value+'px';
		items.push(item);
		sortedDiv.appendChild(elem);
		
		let elemUnsorted = elem.cloneNode();
		elemUnsorted.id = i + 'unsorted';
		unsortedDiv.appendChild(elemUnsorted);
	}
	
	return items;	
}

/*
 * Performs the insertion sort on array and swaps elements
 * accordingly in sortedDiv
 */
function insertionSort(array, sortedDiv){
	for(let i = 0; i < array.length; i++){
		for(let j = i; j > 0; j--){
			if(array[j].value < array[j-1].value){
				swapHTML(array,j-1,j, sortedDiv);
				swapInArray(array,j-1,j);
			}else break;
		}
	}
}

/*
 * Replaces elements in HTML (tough one to figure :)
 */
function swapHTML(array,i,j, sortedDiv){
	let iElem = document.getElementById(array[i].id);
	let jElem = document.getElementById(array[j].id);
	iElem = sortedDiv.replaceChild(jElem,iElem);
	sortedDiv.insertBefore(iElem,jElem.nextSibling);
}

/*
 * Swaps elements in an array
 */
function swapInArray(array,i,j){
	let temp = array[i];
	array[i] = array[j];
	array[j] = temp;
}


